#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void usage(char *prg)
{
	fprintf(stderr, "usage: %s [-h] [<name>]\n", prg);
	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	char *name = "World";

	if (argc > 1) {
		if (strcmp(argv[1], "-h") == 0)
			usage(argv[0]);
		else
			name = argv[1];
	}

	printf("Hello %s!\n", name);

	return 0;
}
