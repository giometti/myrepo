TARGET = helloworld

$(TARGET): $(TARGET).c
	gcc -Wall -O2 -o $(TARGET) $(TARGET).c

clean:
	rm -rf $(TARGET)
